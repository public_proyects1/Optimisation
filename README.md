# Optimisation

This repository contains the work done during the optimisation course in the [University Atonomous of Barcelona](https://mat.uab.cat/~alseda/MasterOpt/index.html). It
contains 3 mains works: 

- The implementation of the A* algorithm for the Routing problem from Barcelona to sevilla.
- Genetic algorithms for the maximization of a complex function. 
- Deterministic optimisation: Rosenbrock's function. 

# Repository structure

The repo is structured as follows: 

```

Optimisation/
  │          ...
  │
  ├─── Astar/        
        │
        ├─── contruct_grap_SPAIN/                         # Code to build and structure the dataset.
        ├─── Images/                         # Images used in the Readme.md
        ├─── main_algorithm_SPAIN/                         # Code of the main algorithm
        ├─── weighting_heuristic/                         # Some Experiments.
        ├─── Assignment-AStar-2020.pdf                         # The Assignment.
        ├─── AStar_report.pdf                         # The Report of this work.
        └─── Readme.md                         # A dedicated Readme.md that explains deeply the content of this folder. 
        
  ├─── deterministic/        
        │
        ├─── results/                         # ipynb with the plots of result.
        ├─── deterministic.c                         # The main code of this work.
        ├─── RosenbrockFunction-revited.pdf                         # The assignment.
        ├─── Deterministic_modelling.pdf                         # The report of this work.
        └─── Readme.md                         # A dedicated Readme.md. 
  ├─── Astar/        
        │
        ├─── images_readme/                         # Code to build and structure the dataset.
        ├─── Plots/                         # Images used in the Readme.md
        ├─── results/                         # Code of the main algorithm
        ├─── rk78/                         # Runge-Kutta.
        ├─── EDO_solver.h                         # Differential equation solver.
        ├─── functions.h                         # Initial and new population generator.
        ├─── main_funcional.c                         # Implementation of the main GA algorithm.
        ├─── Randombits.h                         # binary number generator. 
        ├─── MigrationModel.pdf                         # The assignment.
        ├─── Genetic_algorithm.pdf                         # The report of this work.
        └─── README.md                         # A dedicated Readme.md with the execution instructions.
                                                      
  └─── README.md		      
```
